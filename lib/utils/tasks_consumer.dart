import 'package:todo_list/models/task.dart';
import 'package:todo_list/services/auth_service.dart';
import 'package:todo_list/services/tasks_service.dart';

mixin TasksConsumer {
  final _authService = AuthService();

  Future<TasksService> initTasksService() async {
    final user = await _authService.getCurrentUser();
    final tasksService = TasksService(user: user);
    await tasksService.init();
    return tasksService;
  }

  Future<List<Task>> getTasksList() async {
    final taskService = await initTasksService();
    return await taskService.getAllTasks();
  }

  Future<List<Task>> searchTasks(String search) async {
    final taskService = await initTasksService();
    return await taskService.searchTasks(search);
  }
}
