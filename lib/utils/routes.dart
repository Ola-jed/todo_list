class Routes {
  static const String home = '/';
  static const String login = '/login';
  static const String register = '/register';
  static const String tasks = '/tasks';
  static const String settings = '/settings';
  static const String create = '/create-task';
}
