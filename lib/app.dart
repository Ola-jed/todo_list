import 'package:flutter/material.dart';
import 'package:todo_list/utils/routes.dart';
import 'package:todo_list/views/home_screen.dart';
import 'package:todo_list/views/login_screen.dart';
import 'package:todo_list/views/register_screen.dart';
import 'package:todo_list/views/settings_screen.dart';
import 'package:todo_list/views/task_form.dart';
import 'package:todo_list/views/tasks_list.dart';

MaterialApp app(bool isAuthenticated) {
  return MaterialApp(
    title: 'Todolist',
    theme: ThemeData(
      brightness: MediaQueryData.fromWindow(WidgetsBinding.instance!.window)
          .platformBrightness,
      primaryColor: Colors.teal,
      appBarTheme: const AppBarTheme(
        backgroundColor: Colors.teal,
      ),
      floatingActionButtonTheme: const FloatingActionButtonThemeData(
        backgroundColor: Colors.teal,
      ),
    ),
    initialRoute: isAuthenticated ? Routes.home : Routes.login,
    routes: {
      Routes.login: (context) => const LoginScreen(),
      Routes.register: (context) => const RegisterScreen(),
      Routes.home: (context) => const HomeScreen(),
      Routes.tasks: (context) => const TasksList(),
      Routes.settings: (context) => const SettingsScreen(),
      Routes.create: (context) => const TaskForm(),
    },
    debugShowCheckedModeBanner: false,
  );
}
