import 'package:todo_list/models/serializable.dart';

class User extends Serializable {
  final int? id;
  final String firstName;
  final String lastName;
  final String nickName;
  final String phone;
  final String password;

  User({
    this.id,
    required this.firstName,
    required this.lastName,
    required this.nickName,
    required this.phone,
    required this.password,
  });

  @override
  User.fromMap(Map<String, dynamic> map)
      : id = map['id'] as int?,
        firstName = map['firstName'] as String,
        lastName = map['lastName'] as String,
        nickName = map['nickName'] as String,
        phone = map['phone'] as String,
        password = map['password'] as String;

  @override
  Map<String, dynamic> toMap() => {
        'id': id,
        'firstName': firstName,
        'lastName': lastName,
        'nickName': nickName,
        'phone': phone,
        'password': password
      };

  User copyWith({
    int? id,
    String? firstName,
    String? lastName,
    String? nickName,
    String? phone,
    String? password,
  }) {
    return User(
      id: id ?? this.id,
      firstName: firstName ?? this.firstName,
      lastName: lastName ?? this.lastName,
      nickName: nickName ?? this.nickName,
      phone: phone ?? this.phone,
      password: password ?? this.password,
    );
  }
}
