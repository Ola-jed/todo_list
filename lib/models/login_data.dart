import 'package:todo_list/models/serializable.dart';

class LoginData extends Serializable {
  final String nickName;
  final String password;

  LoginData({
    required this.nickName,
    required this.password,
  });

  @override
  LoginData.fromMap(Map<String, dynamic> map)
      : nickName = map['nickName'] as String,
        password = map['password'] as String;

  @override
  Map<String, dynamic> toMap() => {'nickName': nickName, 'password': password};
}
