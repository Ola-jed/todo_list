import 'package:todo_list/models/serializable.dart';

class Task extends Serializable {
  final int? id;
  final int? userId;
  final String name;
  final String description;
  final int priorityLevel;
  final DateTime startDate;
  final DateTime endDate;

  Task({
    this.id,
    required this.name,
    required this.description,
    required this.priorityLevel,
    required this.startDate,
    required this.endDate,
    this.userId,
  });

  Task copyWith({
    int? id,
    int? userId,
    String? name,
    String? description,
    int? priorityLevel,
    DateTime? startDate,
    DateTime? endDate,
  }) {
    return Task(
      id: id ?? this.id,
      userId: userId ?? this.userId,
      name: name ?? this.name,
      description: description ?? this.description,
      priorityLevel: priorityLevel ?? this.priorityLevel,
      startDate: startDate ?? this.startDate,
      endDate: endDate ?? this.endDate,
    );
  }


  @override
  Task.fromMap(Map<String, dynamic> map)
      : id = map['id'] as int?,
        name = map['name'] as String,
        description = map['description'] as String,
        priorityLevel = map['priorityLevel'] as int,
        startDate = DateTime.parse(map['startDate']),
        endDate = DateTime.parse(map['endDate']),
        userId = map['userId'] as int?;

  @override
  Map<String, dynamic> toMap() => {
        'id': id,
        'name': name,
        'description': description,
        'priorityLevel': priorityLevel,
        'startDate': startDate.toIso8601String(),
        'endDate': endDate.toIso8601String(),
        'userId': userId
      };
}
