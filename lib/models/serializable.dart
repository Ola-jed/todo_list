import 'package:todo_list/models/task.dart';
import 'package:todo_list/models/user.dart';

abstract class Serializable {
  Serializable();

  Serializable.fromMap(final Map<String, dynamic> map);

  Map<String, dynamic> toMap();

  @override
  String toString() {
    return toMap().toString();
  }
}

class SerializableFactory {
  static final Map<Type, Function> _serializableFactories = <Type, Function>{
    User: (final Map<String, dynamic> map) => User.fromMap(map),
    Task: (final Map<String, dynamic> map) => Task.fromMap(map),
  };

  static T fromMap<T extends Serializable>(final Map<String, dynamic> map) {
    return _serializableFactories[T]!(map);
  }
}
