import 'package:flutter/material.dart';
import 'package:todo_list/components/app_drawer.dart';
import 'package:todo_list/components/custom_fab.dart';
import 'package:todo_list/components/dashboard_stat_card.dart';
import 'package:todo_list/services/tasks_service.dart';
import 'package:todo_list/utils/tasks_consumer.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> with TasksConsumer {
  late Future<TasksService> _taskService;

  @override
  void initState() {
    _taskService = initTasksService();
    super.initState();
  }

  Future<void> refresh() async {
    setState(() {
      _taskService = initTasksService();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: const AppDrawer(),
      appBar: AppBar(
        title: const Text('Todolist'),
        elevation: 10,
        actions: <Widget>[
          IconButton(
            onPressed: () {
              showAboutDialog(
                context: context,
                applicationName: 'Todolist',
                applicationVersion: '1.0',
                applicationLegalese: '© Projet Todolist 2022',
                applicationIcon: const Image(
                  image: AssetImage('assets/icon.png'),
                ),
              );
            },
            icon: const Icon(Icons.help),
          )
        ],
      ),
      body: RefreshIndicator(
        onRefresh: refresh,
        child: FutureBuilder<TasksService>(
          future: _taskService,
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting) {
              return const Center(
                child: CircularProgressIndicator(),
              );
            } else if (snapshot.connectionState == ConnectionState.done) {
              if (snapshot.hasData) {
                final data = snapshot.data!;
                final averageDuration = Duration(
                  seconds: data.averageTasksDurationInSeconds().toInt(),
                );
                return SingleChildScrollView(
                  padding: const EdgeInsets.only(top: 15),
                  child: Column(
                    children: [
                      DashboardStatCard(
                        label: 'Nombre total de tâches',
                        value: data.totalCount().toString(),
                      ),
                      DashboardStatCard(
                        label: 'Tâches restantes',
                        value: data.remainingTasksCount().toString(),
                      ),
                      DashboardStatCard(
                        label: 'Tâches prioritaires restantes',
                        value: data.remainingHighPriorityTasks().toString(),
                      ),
                      DashboardStatCard(
                        label: 'Tâches non prioritaires restantes',
                        value: data.remainingLowPriorityTasks().toString(),
                      ),
                      DashboardStatCard(
                        label: 'Durée moyenne des tâches',
                        value: averageDuration
                            .toString()
                            .split('.')
                            .first
                            .padLeft(8, "0"),
                      ),
                    ],
                  ),
                );
              } else {
                return Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      const Text(
                        'Une erreur s\'est produite 🥲',
                        style: TextStyle(fontSize: 18),
                      ),
                      Container(
                        margin: const EdgeInsets.symmetric(vertical: 5),
                        child: ElevatedButton(
                          onPressed: refresh,
                          child: const Text('Réésayer'),
                        ),
                      ),
                    ],
                  ),
                );
              }
            }
            return Container();
          },
        ),
      ),
      floatingActionButton: const CustomFab(),
    );
  }
}
