import 'package:flutter/material.dart';
import 'package:todo_list/components/app_drawer.dart';
import 'package:todo_list/components/custom_fab.dart';
import 'package:todo_list/components/task_widget.dart';
import 'package:todo_list/models/task.dart';
import 'package:todo_list/utils/tasks_consumer.dart';

class TasksList extends StatefulWidget {
  const TasksList({Key? key}) : super(key: key);

  @override
  State<TasksList> createState() => _TasksListState();
}

class _TasksListState extends State<TasksList> with TasksConsumer {
  late Future<List<Task>> _tasks;
  TextEditingController textEditingController = TextEditingController();

  @override
  void initState() {
    _tasks = getTasksList();
    super.initState();
  }

  Future<void> search(String search) async {
    setState(() {
      _tasks = searchTasks(search);
    });
  }

  Future<void> refresh() async {
    setState(() {
      _tasks = getTasksList();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: const AppDrawer(),
      appBar: AppBar(
        title: const Text('Todolist'),
        elevation: 10,
        actions: <Widget>[
          IconButton(
            onPressed: () {
              showAboutDialog(
                context: context,
                applicationName: 'Todolist',
                applicationVersion: '1.0',
                applicationLegalese: '© Projet Todolist 2022',
                applicationIcon: const Image(
                  image: AssetImage('assets/icon.png'),
                ),
              );
            },
            icon: const Icon(Icons.help),
          )
        ],
      ),
      body: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(12.0),
            child: TextField(
              controller: textEditingController,
              decoration: InputDecoration(
                hintText: 'Rechercher une tâche ...',
                prefixIcon: const Icon(Icons.search),
                enabledBorder: OutlineInputBorder(
                  borderSide: const BorderSide(
                    width: 3,
                    color: Colors.black,
                  ),
                  borderRadius: BorderRadius.circular(15),
                ),
                focusedBorder: OutlineInputBorder(
                  borderSide: const BorderSide(
                    width: 3,
                    color: Colors.teal,
                  ),
                  borderRadius: BorderRadius.circular(15),
                ),
                errorBorder: OutlineInputBorder(
                  borderSide: const BorderSide(
                    width: 3,
                    color: Colors.red,
                  ),
                  borderRadius: BorderRadius.circular(15),
                ),
                focusedErrorBorder: OutlineInputBorder(
                  borderSide: const BorderSide(
                    width: 3,
                    color: Colors.red,
                  ),
                  borderRadius: BorderRadius.circular(15),
                ),
              ),
              onChanged: search,
            ),
          ),
          Expanded(
            child: RefreshIndicator(
              onRefresh: refresh,
              child: FutureBuilder<List<Task>>(
                future: _tasks,
                builder: (context, snapshot) {
                  if (snapshot.connectionState == ConnectionState.waiting) {
                    return const Center(
                      child: CircularProgressIndicator(),
                    );
                  } else if (snapshot.connectionState == ConnectionState.done) {
                    if (snapshot.hasData) {
                      final tasks = snapshot.data!;
                      if (tasks.isNotEmpty) {
                        return ListView.builder(
                          itemCount: tasks.length,
                          itemBuilder: (ctx, index) {
                            return TaskWidget(
                              task: tasks[index],
                            );
                          },
                        );
                      } else {
                        return const Center(
                          child: Text(
                            'Aucune tâche',
                            style: TextStyle(fontSize: 18),
                          ),
                        );
                      }
                    } else {
                      return Center(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            const Text(
                              'Une erreur s\'est produite 🥲',
                              style: TextStyle(fontSize: 18),
                            ),
                            Container(
                              margin: const EdgeInsets.symmetric(vertical: 5),
                              child: ElevatedButton(
                                onPressed: refresh,
                                child: const Text('Réésayer'),
                              ),
                            ),
                          ],
                        ),
                      );
                    }
                  }
                  return Container();
                },
              ),
            ),
          ),
        ],
      ),
      floatingActionButton: const CustomFab(),
    );
  }
}
