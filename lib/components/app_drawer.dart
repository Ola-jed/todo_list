import 'package:flutter/material.dart';
import 'package:todo_list/models/user.dart';
import 'package:todo_list/services/auth_service.dart';
import 'package:todo_list/utils/routes.dart';

class AppDrawer extends StatefulWidget {
  const AppDrawer({Key? key}) : super(key: key);

  @override
  State<AppDrawer> createState() => _AppDrawerState();
}

class _AppDrawerState extends State<AppDrawer> {
  final AuthService _authService = AuthService();
  late Future<User> _userFuture;

  @override
  void initState() {
    _userFuture = _authService.getCurrentUser();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Drawer(
      child: ListView(
        children: <Widget>[
          FutureBuilder<User>(
            future: _userFuture,
            builder: (ctx, snapshot) {
              if (snapshot.hasError) {
                return const Center(
                  child: Text('Une erreur s\'est produite 🥲'),
                );
              } else if (snapshot.hasData) {
                final user = snapshot.data!;
                return UserAccountsDrawerHeader(
                  decoration: const BoxDecoration(
                    color: Colors.teal,
                  ),
                  currentAccountPicture: CircleAvatar(
                    radius: 50.0,
                    backgroundColor: Colors.lightGreen,
                    child: Text(
                      user.nickName.substring(0, 2).toUpperCase(),
                      style: const TextStyle(
                        color: Colors.white,
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  accountEmail: Text(user.phone),
                  accountName: Text(user.nickName),
                );
              } else {
                return const Center(
                  child: CircularProgressIndicator(
                    color: Colors.teal,
                  ),
                );
              }
            },
          ),
          Container(
            margin: EdgeInsets.symmetric(vertical: size.height / 200),
            child: ListTile(
              title: const Text('Accueil'),
              leading: const Icon(
                Icons.home,
              ),
              onTap: () {
                Navigator.pushNamed(context, Routes.home);
              },
              tileColor: const Color(0x22222200),
            ),
          ),
          Container(
            margin: EdgeInsets.symmetric(vertical: size.height / 200),
            child: ListTile(
              title: const Text('Liste des tâches'),
              leading: const Icon(
                Icons.list,
              ),
              onTap: () {
                Navigator.pushNamed(context, Routes.tasks);
              },
              tileColor: const Color(0x22222200),
            ),
          ),
          Container(
            margin: EdgeInsets.symmetric(vertical: size.height / 200),
            child: ListTile(
              title: const Text('Paramètres'),
              leading: const Icon(
                Icons.settings,
              ),
              onTap: () {
                Navigator.pushNamed(context, Routes.settings);
              },
              tileColor: const Color(0x22222200),
            ),
          ),
          SizedBox(
            height: size.height / 3,
          ),
          TextButton(
            onPressed: () async {
              await _authService.logout();
              Navigator.pushNamed(context, Routes.login);
            },
            child: const Text(
              'Déconnexion',
              style: TextStyle(color: Colors.teal),
            ),
          ),
          const Center(
            child: Text('Copyright © Projet Todolist 2022'),
          ),
        ],
      ),
    );
  }
}
