import 'package:flutter/material.dart';
import 'package:todo_list/utils/routes.dart';

class CustomFab extends StatelessWidget {
  const CustomFab({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FloatingActionButton(
      onPressed: () {
        Navigator.pushNamed(context, Routes.create);
      },
      child: const Icon(Icons.add),
    );
  }
}
