import 'dart:convert';

import 'package:logging/logging.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:todo_list/models/serializable.dart';
import 'package:todo_list/models/task.dart';
import 'package:todo_list/models/user.dart';

class GenericRepository<T extends Serializable> {
  final _tableName = '$T';
  static const _dbFile = 'todo_list.db';
  final _logger = Logger('$GenericRepository[$T]');
  static Database? _database;

  Future<void> initDatabase() async {
    _logger.info('Initializing database');
    _database ??= await openDatabase(
      join(await getDatabasesPath(), _dbFile),
      onCreate: (database, version) async {
        await database.execute(
          'CREATE TABLE IF NOT EXISTS $User ('
          'id INTEGER PRIMARY KEY AUTOINCREMENT,'
          'lastName TEXT NOT NULL,'
          'firstName TEXT NOT NULL,'
          'nickName TEXT NOT NULL,'
          'phone TEXT NOT NULL,'
          'password TEXT NOT NULL)',
        );
        await database.execute(
          'CREATE TABLE IF NOT EXISTS $Task ('
          'id INTEGER PRIMARY KEY AUTOINCREMENT,'
          'userId INTEGER,'
          'name TEXT NOT NULL,'
          'description TEXT NOT NULL,'
          'priorityLevel INTEGER NOT NULL,'
          'startDate TEXT NOT NULL,'
          'endDate TEXT NOT NULL,'
          'FOREIGN KEY (userId) REFERENCES $User (id) ON DELETE CASCADE ON UPDATE NO ACTION)',
        );
      },
      version: 1,
    );
  }

  static Future<void> initialize() async {
    await (GenericRepository().initDatabase());
  }

  Future<void> insert(T model) async {
    _logger.info(
        'Inserting into database model[$T] : ${jsonEncode(model.toMap())}');
    final dataToInsert = model.toMap();
    final result = await _database?.insert(
      _tableName,
      dataToInsert,
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
    _logger.info('Inserted $result [$T]($result)');
  }

  Future<List<T>> getAll() async {
    _logger.info('Get all models[$T]');
    final data = await _database?.query(_tableName, orderBy: 'id');
    return data?.map((e) => SerializableFactory.fromMap<T>(e)).toList() ?? [];
  }

  Future<T?> findByProperty(String property, dynamic value) async {
    _logger.info('Find [$T] by property $property = $value');
    final data = await _database?.query(
      _tableName,
      where: '$property = ?',
      whereArgs: [value],
    );
    if(data == null || data.isEmpty) {
      return null;
    }
    final model = data.first;
    return SerializableFactory.fromMap<T>(model);
  }

  Future<void> updateById(int id, T model) async {
    await _database?.update(
      '$T',
      model.toMap(),
      where: "id = ?",
      whereArgs: [id],
    );
  }

  Future<void> deleteById(int id) async {
    _logger.info('Deleting model[$T] with id[$id]');
    await _database?.delete(_tableName, where: 'id = ?', whereArgs: [id]);
  }
}
