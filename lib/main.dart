import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:logging/logging.dart';
import 'package:todo_list/app.dart';
import 'package:todo_list/repositories/generic_repository.dart';
import 'package:todo_list/services/auth_service.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await GenericRepository.initialize();
  Logger.root.level = Level.ALL;
  Logger.root.onRecord.listen((record) {
    if (kDebugMode) {
      print('${record.level.name}: ${record.time}: ${record.message}');
    }
  });
  final authService = AuthService();
  final isAuthenticated = await authService.isAuthenticated();
  runApp(app(isAuthenticated));
}