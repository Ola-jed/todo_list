import 'package:logging/logging.dart';
import 'package:todo_list/models/task.dart';
import 'package:todo_list/models/user.dart';
import 'package:todo_list/repositories/generic_repository.dart';

class TasksService {
  final _tasksRepository = GenericRepository<Task>();
  List<Task> _allTasks = [];
  final User user;

  TasksService({required this.user});

  Future<void> init() async {
    _allTasks = (await _tasksRepository.getAll())
        .where((element) => element.userId == user.id)
        .toList();
    Logger.root.info(_allTasks);
  }

  int totalCount() {
    return _allTasks.length;
  }

  int completedTasksCount() {
    return _allTasks.where(_isCompletedTask).length;
  }

  int remainingTasksCount() {
    return _allTasks.where(_isRemainingTask).length;
  }

  int remainingHighPriorityTasks() {
    return _allTasks
        .where((element) =>
            element.priorityLevel >= 5 && _isRemainingTask(element))
        .length;
  }

  int remainingLowPriorityTasks() {
    return _allTasks
        .where(
            (element) => element.priorityLevel < 5 && _isRemainingTask(element))
        .length;
  }

  double averageTasksDurationInSeconds() {
    final now = DateTime.now();
    final finishedTasks =
        _allTasks.where((element) => element.endDate.isBefore(now));
    if (finishedTasks.isEmpty) {
      return 0;
    }
    final totalDuration = finishedTasks
        .map((e) => e.endDate.difference(e.startDate))
        .fold(Duration.zero,
            (Duration previousValue, element) => previousValue + element);
    return totalDuration.inSeconds / finishedTasks.length;
  }

  bool _isRemainingTask(Task task) {
    return task.startDate == task.endDate ||
        task.endDate.isAfter(DateTime.now());
  }

  bool _isCompletedTask(Task task) {
    return task.startDate != task.endDate &&
        task.endDate.isBefore(DateTime.now());
  }

  Future<List<Task>> getAllTasks() {
    return Future.sync(() => _allTasks);
  }

  Future<List<Task>> searchTasks(String search) {
    return Future.sync(
      () => _allTasks
          .where((task) =>
              task.name.contains(search) || task.description.contains(search))
          .toList(),
    );
  }
}
