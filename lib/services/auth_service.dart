import 'dart:async';
import 'dart:convert';

import 'package:crypt/crypt.dart';
import 'package:logging/logging.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:todo_list/models/login_data.dart';
import 'package:todo_list/models/user.dart';
import 'package:todo_list/repositories/generic_repository.dart';

class AuthService {
  final _repository = GenericRepository<User>();
  static const String _authenticated = 'authenticated';
  static const String _currentUser = 'current-user';
  static User? _user;

  Future<User?> register(User user) async {
    final userToInsert = user.copyWith(
      password: Crypt.sha256(user.password).toString(),
    );
    await _repository.insert(userToInsert);
    final sharedPreferences = await SharedPreferences.getInstance();
    await sharedPreferences.setBool(_authenticated, true);
    final userInserted =
        await _repository.findByProperty('phone', userToInsert.phone);
    await sharedPreferences.setString(
      _currentUser,
      jsonEncode(userInserted?.toMap()),
    );
    return userInserted;
  }

  Future<bool> login(LoginData loginData) async {
    final user = await _repository.findByProperty(
      'nickName',
      loginData.nickName,
    );
    if (user == null) {
      return false;
    }
    final sharedPreferences = await SharedPreferences.getInstance();
    if (Crypt(user.password).match(loginData.password)) {
      await sharedPreferences.setBool(_authenticated, true);
      return true;
    }
    return false;
  }

  Future<void> logout() async {
    final sharedPreferences = await SharedPreferences.getInstance();
    _user = null;
    await sharedPreferences.remove(_authenticated);
  }

  Future<bool> isAuthenticated() async {
    final sharedPreferences = await SharedPreferences.getInstance();
    return sharedPreferences.getBool(_authenticated) ?? false;
  }

  Future<User> getCurrentUser() async {
    if (_user == null) {
      final sharedPreferences = await SharedPreferences.getInstance();
      final serializedUser = sharedPreferences.getString(_currentUser);
      _user = User.fromMap(jsonDecode(serializedUser!));
    }
    return _user!;
  }

  Future<void> updateCurrentUser(User newUserData) async {
    final pwd = newUserData.password.trim().isEmpty
        ? _user!.password
        : Crypt.sha256(newUserData.password).toString();
    final userToInsert = newUserData.copyWith(id: _user!.id, password: pwd);
    await _repository.updateById(_user!.id!, userToInsert);
    _user = userToInsert;
    Logger.root.info('Updating user : ${_user.toString()}');
  }
}
